package firebase

import (
	"gitlab.com/golang12/push_server/modal/firebase"
	"gitlab.com/golang12/push_server/request"
	"gitlab.com/golang12/queue_action"
	"net/http"
)

func Distribution(b []byte, message interface{}) {
	switch message.(type) {
	case firebase.PushToken:
		nr := &request.NetRequest{
			R: &http.Request{},
		}
		go PushToken(nr, b, message.(firebase.PushToken))
	case firebase.PushTopic:
		nr := &request.NetRequest{
			R: &http.Request{},
		}
		go PushTopic(nr, b)
	case firebase.RequestSubscribe:
		ru := message.(firebase.RequestSubscribe)
		topics := ru.Topics
		for _, v := range topics {
			sub := firebase.Subscribe{}
			sub.Topic = v
			sub.Tokens = ru.Tokens
			b, _ := sub.MarshalJSON()
			nr := &request.NetRequest{
				R: &http.Request{},
			}
			asu := &ActionSub{
				Sub:     sub,
				Body:    b,
				Request: nr,
			}
			go queue.Add(ru.Key, asu)
		}
	case firebase.RequestUnsubscribe:
		ru := message.(firebase.RequestUnsubscribe)
		topics := ru.Topics
		for _, v := range topics {
			unsub := firebase.Unsubscribe{}
			unsub.Topic = v
			unsub.Tokens = ru.Tokens
			b, _ := unsub.MarshalJSON()
			nr := &request.NetRequest{
				R: &http.Request{},
			}
			asu := &ActionUnsub{
				Unsub:   unsub,
				Body:    b,
				Request: nr,
			}
			go queue.Add(ru.Key, asu)
		}
	}
}
