package handler

import (
	"github.com/mailru/easyjson/jlexer"
	firebase2 "gitlab.com/golang12/push_server/firebase"
	"gitlab.com/golang12/push_server/logs"
	"gitlab.com/golang12/push_server/modal/firebase"
	"gitlab.com/golang12/push_server/response"
	"io/ioutil"
	"net/http"
)

func PushToken(w http.ResponseWriter, r *http.Request) {
	b, err := ioutil.ReadAll(r.Body)
	if err == nil {
		push := firebase.PushToken{}
		r := jlexer.Lexer{Data: b}
		push.UnmarshalEasyJSON(&r)
		err := r.Error()
		if err != nil {
			err := response.JsonResponse(w, 500, []byte(err.Error()))
			if err != nil {
				logs.LogPush(err.Error(), "/var/www/push/logs/system.out")
			}
		}
		go firebase2.Distribution(b, push)
		if err != nil {
			err := response.JsonResponse(w, 500, []byte(err.Error()))
			if err != nil {
				logs.LogPush(err.Error(), "/var/www/push/logs/system.out")
			}
		}
	} else {
		logs.LogPush(err.Error(), "/var/www/push/logs/system.out")
	}
}

func PushTopic(w http.ResponseWriter, r *http.Request) {
	b, err := ioutil.ReadAll(r.Body)
	if err == nil {
		push := firebase.PushTopic{}
		r := jlexer.Lexer{Data: b}
		push.UnmarshalEasyJSON(&r)
		err := r.Error()
		if err != nil {
			err := response.JsonResponse(w, 500, []byte(err.Error()))
			if err != nil {
				logs.LogPush(err.Error(), "/var/www/push/logs/system.out")
			}
		}
		go firebase2.Distribution(b, push)
		if err != nil {
			err := response.JsonResponse(w, 500, []byte(err.Error()))
			if err != nil {
				logs.LogPush(err.Error(), "/var/www/push/logs/system.out")
			}
		}
	} else {
		logs.LogPush(err.Error(), "/var/www/push/logs/system.out")
	}
}

func Subscribe(w http.ResponseWriter, r *http.Request) {
	b, err := ioutil.ReadAll(r.Body)
	if err == nil {
		push := firebase.RequestSubscribe{}
		r := jlexer.Lexer{Data: b}
		push.UnmarshalEasyJSON(&r)
		err := r.Error()
		if err != nil {
			err := response.JsonResponse(w, 500, []byte(err.Error()))
			if err != nil {
				logs.LogPush(err.Error(), "/var/www/push/logs/system.out")
			}
		}
		go firebase2.Distribution(b, push)
		if err != nil {
			err := response.JsonResponse(w, 500, []byte(err.Error()))
			if err != nil {
				logs.LogPush(err.Error(), "/var/www/push/logs/system.out")
			}
		}
	} else {
		logs.LogPush(err.Error(), "/var/www/push/logs/system.out")
	}
}

func Unsubscribe(w http.ResponseWriter, r *http.Request) {
	b, err := ioutil.ReadAll(r.Body)
	if err == nil {
		push := firebase.RequestUnsubscribe{}
		r := jlexer.Lexer{Data: b}
		push.UnmarshalEasyJSON(&r)
		err := r.Error()
		if err != nil {
			err := response.JsonResponse(w, 500, []byte(err.Error()))
			if err != nil {
				logs.LogPush(err.Error(), "/var/www/push/logs/system.out")
			}
		}
		go firebase2.Distribution(b, push)
		if err != nil {
			err := response.JsonResponse(w, 500, []byte(err.Error()))
			if err != nil {
				logs.LogPush(err.Error(), "/var/www/push/logs/system.out")
			}
		}
	} else {
		logs.LogPush(err.Error(), "/var/www/push/logs/system.out")
	}
}