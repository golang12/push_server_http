package request

import (
	"bytes"
	"gitlab.com/golang12/push_server/response"
	"io/ioutil"
	"net/http"
	url2 "net/url"
)

type NetRequest struct {
	R *http.Request
}

type GenNetRequest struct{}

func (gen GenNetRequest) New() Request {
	return &NetRequest{
		&http.Request{},
	}
}

func (nr *NetRequest) SetUrl(url string) {
	nr.R.URL, _ = url2.ParseRequestURI(url)
}

func (nr *NetRequest) SetMethod(method string) {
	nr.R.Method = method
}

func (nr *NetRequest) SetHeader(header string, value string) {
	if nr.R.Header == nil {
		nr.R.Header = make(http.Header)
	}
	nr.R.Header.Add(header, value)
}

func (nr *NetRequest) SetBody(body []byte) {
	b := bytes.NewBuffer(body)
	r := ioutil.NopCloser(b)
	nr.R.Body = r
}

func (nr *NetRequest) Do() (error, response.Response) {
	c := http.Client{}
	r := &response.NetResponse{}
	resp, err := c.Do(nr.R)
	r.Response = resp
	if err != nil {
		return err, r
	} else {
		r.Response = resp
		return nil, r
	}
}
