package main

import (
	"bytes"
	"fmt"
	"gitlab.com/golang12/push_server/modal/firebase"
	"io/ioutil"
	"log"
	"net/http"
	"testing"
)

func TestPushTopic(t *testing.T) {
	topic := "/topics/test_2"
	title := "test"
	body := "test"

	pushToic := firebase.GetPushTopic(topic, title, body)

	req, err := http.NewRequest("POST", "http://localhost:8000/firebase/push/topic", bytes.NewBuffer(pushToic))
	if err != nil {
		log.Fatal("Error reading request. ", err)
	}

	req.Header.Set("Content-Type", "application/json")
	client := &http.Client{}

	resp, err := client.Do(req)
	if err != nil {
		log.Fatal("Error reading response. ", err)
	}
	defer resp.Body.Close()

	fmt.Println("response Status:", resp.Status)
	fmt.Println("response Headers:", resp.Header)

	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatal("Error reading body. ", err)
	}

	fmt.Printf("%s\n", string(b[:]))
}

func TestUnsub(t *testing.T) {
	tokens := []string{
		"fTf9W8Ffp6g:APA91bHGLb198DxEGwmwIG-dIGsneEDt8kdK6hB_dJKnefA-aO2awbwgPp9uxMXULS30LQS757--WakdyuMaqt_icELeXavw7-e5Es8itZiuAAfJkFs2KITL3lIgiedWrruzjMQpdBdG",
	}
	topic := []string{"/topics/test_1","/topics/test_2"}

	sub := firebase.GetUnsubscribe(tokens, topic)
	req, err := http.NewRequest("POST", "http://localhost:8000/firebase/unsubscribe", bytes.NewBuffer(sub))
	if err != nil {
		log.Fatal("Error reading request. ", err)
	}

	req.Header.Set("Content-Type", "application/json")
	client := &http.Client{}

	resp, err := client.Do(req)
	if err != nil {
		log.Fatal("Error reading response. ", err)
	}
	defer resp.Body.Close()

	fmt.Println("response Status:", resp.Status)
	fmt.Println("response Headers:", resp.Header)

	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatal("Error reading body. ", err)
	}

	fmt.Printf("%s\n", string(b[:]))
}

func TestSub(t *testing.T) {
	tokens := []string{
		"fTf9W8Ffp6g:APA91bHGLb198DxEGwmwIG-dIGsneEDt8kdK6hB_dJKnefA-aO2awbwgPp9uxMXULS30LQS757--WakdyuMaqt_icELeXavw7-e5Es8itZiuAAfJkFs2KITL3lIgiedWrruzjMQpdBdG",
	}
	topic := []string{"/topics/test_1","/topics/test_2"}

	sub := firebase.GetSubscribe(tokens, topic)
	req, err := http.NewRequest("POST", "http://localhost:8000/firebase/subscribe", bytes.NewBuffer(sub))
	if err != nil {
		log.Fatal("Error reading request. ", err)
	}

	req.Header.Set("Content-Type", "application/json")
	client := &http.Client{}

	resp, err := client.Do(req)
	if err != nil {
		log.Fatal("Error reading response. ", err)
	}
	defer resp.Body.Close()

	fmt.Println("response Status:", resp.Status)
	fmt.Println("response Headers:", resp.Header)

	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatal("Error reading body. ", err)
	}

	fmt.Printf("%s\n", string(b[:]))
}

func TestPushToken(t *testing.T) {
	token := []string{"fTf9W8Ffp6g:APA91bHGLb198DxEGwmwIG-dIGsneEDt8kdK6hB_dJKnefA-aO2awbwgPp9uxMXULS30LQS757--WakdyuMaqt_icELeXavw7-e5Es8itZiuAAfJkFs2KITL3lIgiedWrruzjMQpdBdG"}
	title := "test"
	body := "test"

	pushToic := firebase.GetPushToken(token, title, body)
	req, err := http.NewRequest("POST", "http://localhost:8000/firebase/push/token", bytes.NewBuffer(pushToic))
	if err != nil {
		log.Fatal("Error reading request. ", err)
	}

	req.Header.Set("Content-Type", "application/json")
	client := &http.Client{}

	resp, err := client.Do(req)
	if err != nil {
		log.Fatal("Error reading response. ", err)
	}
	defer resp.Body.Close()

	fmt.Println("response Status:", resp.Status)
	fmt.Println("response Headers:", resp.Header)

	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatal("Error reading body. ", err)
	}

	fmt.Printf("%s\n", string(b[:]))
}
