package main

import (
	"github.com/gorilla/mux"
	"gitlab.com/golang12/push_server/firebase"
	"gitlab.com/golang12/push_server/handler"
	"net/http"
	"os"
)

func main() {
	err := os.Setenv("FIREBASE_ENV", "key=")
	if err != nil {
		panic(err)
	}

	data := map[string]interface{}{
		"host":     "localhost",
		"db":       "",
		"port":     "3306",
		"username": "",
		"password": "",
	}
	go firebase.HandlerInvalidToken(data)

	router := mux.NewRouter()
	router.HandleFunc("/firebase/push/token", handler.PushToken).Methods("POST")
	router.HandleFunc("/firebase/subscribe", handler.Subscribe).Methods("POST")
	router.HandleFunc("/firebase/unsubscribe", handler.Unsubscribe).Methods("POST")
	router.HandleFunc("/firebase/push/topic", handler.PushTopic).Methods("POST")
	http.Handle("/", router)
	http.ListenAndServe(":8000", nil)
}
