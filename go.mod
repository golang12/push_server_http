module gitlab.com/golang12/push_server

require (
	github.com/go-sql-driver/mysql v1.5.0
	github.com/gorilla/mux v1.7.4
	github.com/mailru/easyjson v0.7.1
	gitlab.com/golang12/log v1.3.1
	gitlab.com/golang12/queue_action v1.0.1
)

go 1.14
