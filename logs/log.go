package logs

import (
	logs "gitlab.com/golang12/log"
)

func LogPush(data string, path string) {
	output := &logs.OutputFile{Path: path}
	log := logs.New(output)
	log.SetFullTrace(false)
	log.SetDepth(2)
	log.SetData(data)
	log.Log()
}
