package response

import (
	"net/http"
)

type Response interface {
	GetCode() int
	GetBody() ([]byte, error)
}

func JsonResponse(w http.ResponseWriter, code int, message []byte) error {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	_, err := w.Write(message)
	if err != nil {
		return err
	} else {
		return nil
	}
}
