package modal

import (
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
	"strings"
)

func DeleteTokens(tokens []string, config map[string]interface{}) {
	db, err := sql.Open("mysql", config["username"].(string)+":"+config["password"].(string)+"@tcp("+config["host"].(string)+":"+config["port"].(string)+")/"+config["db"].(string))
	if err != nil {
		panic(err)
	}

	defer db.Close()

	for i := range tokens {
		tokens[i] = "'" + tokens[i] + "'"
	}

	q := "DELETE FROM fcm_token WHERE token IN (" + strings.Join(tokens, ",") + ")"
	_, err = db.Exec(q)

	if err != nil {
		panic(err)
	}
}
