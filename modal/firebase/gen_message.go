package firebase

func GetPushToken(tokens []string, title string, body string) []byte {
	notif := Notification{}
	notif.Body = body
	notif.Title = title

	push := PushToken{}
	push.Notification = notif
	push.Tokens = tokens
	b, e := push.MarshalJSON()
	if e != nil {
		panic(e)
	}
	return b
}

func GetPushTopic(topic string, title string, body string) []byte {
	notif := Notification{}
	notif.Body = body
	notif.Title = title

	push := PushTopic{}
	push.Notification = notif
	push.Topic = topic
	b, e := push.MarshalJSON()
	if e != nil {
		panic(e)
	}
	return b
}

func GetUnsubscribe(tokens []string, topic []string) []byte {
	unsub := RequestUnsubscribe{}
	unsub.Topics = topic
	unsub.Tokens = tokens
	unsub.Key = 1
	b, e := unsub.MarshalJSON()
	if e != nil {
		panic(e)
	}
	return b
}

func GetSubscribe(tokens []string, topic []string) []byte {
	sub := RequestUnsubscribe{}
	sub.Topics = topic
	sub.Tokens = tokens
	sub.Key = 1
	b, e := sub.MarshalJSON()
	if e != nil {
		panic(e)
	}
	return b
}
