package firebase

type RequestSubscribe struct {
	Key    int `json:"key"`
	Topics []string `json:"topics"`
	Tokens  []string `json:"tokens"`
}

type RequestUnsubscribe struct {
	Key    int `json:"key"`
	Topics []string `json:"topics"`
	Tokens  []string `json:"tokens"`
}

type PushTopic struct {
	Topic        string       `json:"to"`
	Notification Notification `json:"notification"`
}

type PushToken struct {
	Tokens       []string     `json:"registration_ids"`
	Notification Notification `json:"notification"`
}

type Notification struct {
	Title string `json:"title"`
	Body  string `json:"body"`
}

type Subscribe struct {
	Topic  string   `json:"to"`
	Tokens []string `json:"registration_tokens"`
}

type Unsubscribe struct {
	Topic  string   `json:"to"`
	Tokens []string `json:"registration_tokens"`
}

type Response struct {
	MulticastId int                 `json:"multicast_id"`
	CanonicalId int                 `json:"canonical_ids"`
	Failure     int                 `json:"failure"`
	Success     int                 `json:"success"`
	Result      []map[string]string `json:"results"`
}
